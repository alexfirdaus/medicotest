﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MedicoTest.Models;

namespace MedicoTest.Controllers
{
    [Route("api/events")]
    [ApiController]
    public class EventsControllers:ControllerBase
    {
        private readonly EventContext _context;

        public EventsControllers(EventContext context)
        {
            _context = context;

            if (_context.Events.Count() == 0)
            {
                _context.Events.Add(new Event { Name = "Event1" });
                _context.SaveChanges();
            }
        }

        [HttpGet]
        public ActionResult<List<Event>> GetAll()
        {
            return _context.Events.ToList();
        }

        [HttpGet("{id}", Name = "GetEvent")]
        public ActionResult<Event> GetById(int id)
        {
            var item = _context.Events.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

        [HttpPost]
        public IActionResult Create(Event item)
        {
            _context.Events.Add(item);
            _context.SaveChanges();

            return CreatedAtRoute("GetEvent", new { id = item.Id }, item);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, Event item)
        {
            var eventItem = _context.Events.Find(id);
            if (eventItem == null)
            {
                return NotFound();
            }

            eventItem.Status = item.Status;
            eventItem.Name = item.Name;
            eventItem.DateStart = item.DateStart;
            eventItem.DateEnd = item.DateEnd;

            _context.Events.Update(eventItem);
            _context.SaveChanges();
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var todo = _context.Events.Find(id);
            if (todo == null)
            {
                return NotFound();
            }

            _context.Events.Remove(todo);
            _context.SaveChanges();
            return NoContent();
        }
    }
}
